package com.onlineshop.shop;

public abstract class CartItem {

	private int quantity;
	private Product product;	

	public CartItem(Product product, int quantity) {
		this.quantity = quantity;
		this.product = product;
	}

	public Product getProduct() {
		return this.product;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public int unitPrice() {
		return this.product.unitPrice();
	}
	
	public void increaseQty(int quantity){
		this.quantity += quantity;
	}
	
	public void decreaseQty(int quantity){
		this.quantity -= quantity;
	}
	
	abstract public int price();
}
