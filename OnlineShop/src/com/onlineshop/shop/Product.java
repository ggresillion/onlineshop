package com.onlineshop.shop;

public class Product {

	private String description;

	private int unitPrice;

	public Product(String description, int unitPrice) {
		this.description = description;
		this.unitPrice = unitPrice;
	}

	public String getDescription() {

		return this.description;

	}

	public int unitPrice() {

		return this.unitPrice;
	}

}
