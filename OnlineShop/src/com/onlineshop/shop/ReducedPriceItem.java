	package com.onlineshop.shop;

public class ReducedPriceItem extends CartItem{
	
	int priceFactor;
	
	public ReducedPriceItem(Product product, int quantity, int priceFactor) {
		super(product, quantity);
		this.priceFactor = priceFactor;
	}

	@Override
	public int price() {
		
		int price = this.getProduct().unitPrice() * this.getQuantity();
		int reduce = (price * this.priceFactor)/100;
		price = price - reduce;
		return (price);
	}

}