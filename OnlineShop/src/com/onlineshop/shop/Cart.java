package com.onlineshop.shop;

import java.util.ArrayList;

public class Cart {

	private ArrayList<CartItem> cartList = new ArrayList<CartItem>();

	public void clear() {

		cartList.clear();

	}

	public void add(CartItem item) {

		cartList.add(item);

	}

	public void remove(CartItem item) {

		cartList.remove(item);
	}

	public int total() {

		int prix;

		prix = 0;

		for (int i = 0; i < cartList.size(); i++) {
			prix = prix + cartList.get(i).getProduct().unitPrice() * cartList.get(i).getQuantity();
		}

		return prix;

	}

	public int quantityProduct(Product p) {

		int quantityPrice;

		quantityPrice = 0;

		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProduct().getDescription() == p.getDescription()) {

				quantityPrice = quantityPrice + p.unitPrice() * cartList.get(i).getQuantity();
			}
		}

		return quantityPrice;

	}

	public ArrayList<CartItem> priceGreaterThan(int amount) {

		ArrayList<CartItem> greaterArray = new ArrayList<CartItem>();

		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProduct().unitPrice() > amount) {
				greaterArray.add(cartList.get(i));
			}
		}

		return greaterArray;

	}

	@Override
	public String toString() {

		String detail = "";

		for (int i = 0; i < cartList.size(); i++) {
			switch (cartList.get(i).getClass().toString()) {

			case "RegularItem":

				detail = detail + "Regular Item ";

				break;
			case "ReducedPriceItem":

				detail = detail + "Reduce Item ";

				break;
			case "DegressiveItem":

				detail = detail + "Degressive Item ";

				break;
			case "BundledItem":

				detail = detail + "Bundled Item ";

				break;
			}
			
			detail = detail + " item : "+ cartList.get(i).getProduct().getDescription() + " qte : " + cartList.get(i).getQuantity() + " up : " + cartList.get(i).getProduct().unitPrice() + "   " + quantityProduct(cartList.get(i).getProduct()) + "\n"  ;
			 
		
			
			
		}
		
		return detail;

		
	}

}
