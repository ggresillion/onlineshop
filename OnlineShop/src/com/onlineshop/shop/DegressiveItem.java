package com.onlineshop.shop;

public class DegressiveItem extends CartItem {

	int thresholdQuantity;
	int reducedUnitPrice;

	public DegressiveItem(Product product, int quantity, int thresholdQuantity, int reducedUnitPrice) {
		super(product, quantity);
		this.thresholdQuantity = thresholdQuantity;
		this.reducedUnitPrice = reducedUnitPrice;
	}

	@Override
	public int price() {
		if (this.getQuantity() >= this.thresholdQuantity) {
			return this.reducedUnitPrice * this.getQuantity();
		} else {
			return this.getProduct().unitPrice() * this.getQuantity();
		}
	}

}
