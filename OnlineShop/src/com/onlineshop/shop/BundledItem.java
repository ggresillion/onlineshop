package com.onlineshop.shop;

public class BundledItem extends CartItem {
	
	int bundleSize;
	int reducedUnitPrice;
	
	public BundledItem(Product product, int quantity, int bundleSize, int reducedUnitPrice) {
		super(product, quantity);
		this.bundleSize = bundleSize;
		this.reducedUnitPrice = reducedUnitPrice;
	}

	@Override
	public int price() {
		return this.bundleSize * this.reducedUnitPrice;
	}
}
