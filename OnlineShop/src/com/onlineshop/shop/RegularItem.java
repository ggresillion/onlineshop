package com.onlineshop.shop;

public class RegularItem extends CartItem{


	public RegularItem(Product product, int quantity) {
		super(product, quantity);
	}

	@Override
	public int price() {
		return this.getProduct().unitPrice() * this.getQuantity();
	}

}
