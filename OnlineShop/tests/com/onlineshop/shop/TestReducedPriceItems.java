package com.onlineshop.shop;

import org.junit.Before;
import org.junit.Test;

import com.onlineshop.shop.Cart;
import com.onlineshop.shop.CartItem;
import com.onlineshop.shop.Product;
import com.onlineshop.shop.ReducedPriceItem;

import static org.junit.Assert.assertEquals;

public class TestReducedPriceItems {

    private Product product;

    @Before
    public void setUp() {
        product = new Product("something", 1000);
    }

    @Test
    public void test_promotionAppliesToOne() {
        CartItem reducedItem = new ReducedPriceItem(product, 1, 15);
        assertEquals(850, reducedItem.price(), 0.0);
    }
}
